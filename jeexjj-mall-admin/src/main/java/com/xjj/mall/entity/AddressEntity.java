/****************************************************
 * Description: Entity for t_mall_address
 * Copyright:   Copyright (c) 2018
 * Company:     xjj
 * @author      zhanghejie
 * @version     1.0
 * @see
	HISTORY
    *  2018-09-13 zhanghejie Create File
**************************************************/

package com.xjj.mall.entity;

import com.xjj.framework.entity.EntitySupport;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class AddressEntity extends EntitySupport {

    private static final long serialVersionUID = 1L;
    public AddressEntity(){}
    private Long userId;//user_id
    private String userName;//user_name
    private String tel;//tel
    private String streetName;//street_name
    private Boolean isDefault;//is_default
    
    /**
     * 返回user_id
     * @return user_id
     */
    public Long getUserId() {
        return userId;
    }
    
    /**
     * 设置user_id
     * @param userId user_id
     */
    public void setUserId(Long userId) {
        this.userId = userId;
    }
    
    /**
     * 返回user_name
     * @return user_name
     */
    public String getUserName() {
        return userName;
    }
    
    /**
     * 设置user_name
     * @param userName user_name
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }
    
    /**
     * 返回tel
     * @return tel
     */
    public String getTel() {
        return tel;
    }
    
    /**
     * 设置tel
     * @param tel tel
     */
    public void setTel(String tel) {
        this.tel = tel;
    }
    
    /**
     * 返回street_name
     * @return street_name
     */
    public String getStreetName() {
        return streetName;
    }
    
    /**
     * 设置street_name
     * @param streetName street_name
     */
    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }
    
    /**
     * 返回is_default
     * @return is_default
     */
    public Boolean getIsDefault() {
        return isDefault;
    }
    
    /**
     * 设置is_default
     * @param isDefault is_default
     */
    public void setIsDefault(Boolean isDefault) {
        this.isDefault = isDefault;
    }
    

    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.SIMPLE_STYLE).append("com.xjj.mall.entity.AddressEntity").append("ID="+this.getId()).toString();
    }
}

