<#--
/****************************************************
 * Description: 内容分类的简单列表页面，没有编辑功能
 * Copyright:   Copyright (c) 2018
 * Company:     xjj
 * @author      zhanghejie
 * @version     1.0
 * @see
	HISTORY
    *  2018-09-13 zhanghejie Create File
**************************************************/
-->
<#include "/templates/xjj-list.ftl"> 
<@list id=tabId>
	<thead>
		<tr>
			<th><input type="checkbox" class="bscheckall"></th>
	        <th>板块名称</th>
	        <th>类型 0轮播图 1板块种类一 2板块种类二 3板块种类三 </th>
	        <th>排列序号</th>
	        <th>所属位置 0首页 1商品推荐 2我要捐赠</th>
	        <th>板块限制商品数量</th>
	        <th>状态</th>
	        <th>备注</th>
	        <th>创建时间</th>
	        <th>更新时间</th>
	        <th>操作</th>
		</tr>
	</thead>
	<tbody>
		<#list page.items?if_exists as item>
		<tr>
			<td>
			<input type="checkbox" class="bscheck" data="id:${item.id}">
			</td>
			<td>
			    ${item.name}
			</td>
			<td>
			    ${item.type}
			</td>
			<td>
			    ${item.sortOrder}
			</td>
			<td>
			    ${item.position}
			</td>
			<td>
			    ${item.limitNum}
			</td>
			<td>
			    ${item.status}
			</td>
			<td>
			    ${item.remark}
			</td>
			<td>
				<#if item.created?exists>
			    ${item.created?string('yyyy-MM-dd HH:mm:ss')}
			    </#if>
			</td>
			<td>
				<#if item.updated?exists>
			    ${item.updated?string('yyyy-MM-dd HH:mm:ss')}
			    </#if>
			</td>
			<td>
            	<@button type="purple" icon="fa fa-pencil" onclick="XJJ.edit('${base}/mall/panel/input/${item.id}','修改内容分类','${tabId}');">修改</@button>
				<@button type="danger" icon=" fa fa-trash-o" onclick="XJJ.del('${base}/mall/panel/delete/${item.id}','删除内容分类？',false,{id:'${tabId}'});">删除</@button>
            </td>
		</tr>
		</#list>
	</tbody>
</@list>